codelab1 :: [Int] -> Int -> Int
codelab1 array k = maximum $ trailingZeroes array k

trailingZeros :: [Int] -> Int
trailingZeroes nums = trailingZeros' $ product nums

pow5 = [5,25..]
zero = 0
index = 0

trailingZeros' :: Int -> Int
trailingZeros num 
        | num < 0 = zero  
        | otherwise = num / (zero !! index)
