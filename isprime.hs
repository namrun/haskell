divides :: Int -> Int -> Bool
divides d n = rem n d == 0

firstFactorAfter :: Int -> Int -> Int
firstFactorAfter k n | divides k n = k
                     | k*k > n         = n
                     | otherwise   = ldf(k+1) n

--The use of auxillary functions or helper functions ensures it is easier to work with and deal
--There is a sudden increase in readability as it simplifies the work and makes it easier to read

firstFactor :: Int -> Int
firstFactor n = ldf 2 n

isPrime :: Int -> Bool
isPrime n | n < 0 = error "Nope"
isPrime n | n == 1 = False
isPrime n | otherwise = firstFactorAfter n == n

--if the first divisor is the number itself then it is prime
-- that is what is happening here
-- run till sqrt(n), as after that all is a repetition
-- if the function returns n then it is a prime for the range(2, sqrt(n))
    
main = do
    n <- getLine
    n :: Int 
    print(isPrime n)
